This is an adaptation of the **Wikistatsextractor** to suit the requirements of the GELM application :

*https://github.com/diffbot/wikistatsextractor
*https://gitlab.com/jact/GELM

In order to use the extractor you should follow the same steps of the original one:

## How to use it?

You will need at least 14GB of RAM to execute this program, and around 100GB of SSD
- Clone this git
- Download the latest wikipedia dump https://dumps.wikimedia.org/enwiki/latest/, look for enwiki-latest-pages-articles.xml.bz2. Uncompress it.
- (If not done already download maven)
- Compile (mvn compile)
- checkout the configuration file in /conf. Adapt it to point to the uncompressed dump
- Extend the RAM used by java to at least 14GB: ```export MAVEN_OPTS="-Xmx32g"```
- Run the script. From the project root: ```mvn exec:java```
- The script will take around 30 min to produce the output (by default in data/output), and some temporary files (among others, the redirections) in the tmp folder (by default in data/tmp).

After that you should execute the CreateDataBase program, to produce the GELM database. This program needs the H2 database *jar* in its ClassPath.

* http://www.h2database.com/html/download.html



## License 
The MIT License (MIT)

Copyright (c) 2015 Diffbot

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
