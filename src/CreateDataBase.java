
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class to create the required database from the text files created by the
 * extractor This classes uses H2 open source database in embedded mode.
 * http://www.h2database.com/html/main.html This will create five tables:
 * Article_Counts : Each article appearances and context Match: Token related to
 * a list of multitokens starting with the same token Multitoken_Dict:
 * Multitoken anchor count, token count and related articles StopWords :
 * Stopwords lists for different languages. Metadata: The total article count,
 * reference count , language and data.
 * 
 */

public class CreateDataBase {

	static Connection con;
	static Statement stmt;
	static HashMap<String, Integer> stopWords = new HashMap<String, Integer>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {

			long startTime = System.currentTimeMillis();
			Class.forName("org.h2.Driver");
			con = DriverManager.getConnection(
					"jdbc:h2:/gscratch2/users/jcampos004/data/db/Hugedatabase",
					"", "");
			stmt = con.createStatement();
			long endTime = System.currentTimeMillis();
			long total = (endTime - startTime);
			System.out.println("Connection time " + total);


			//stmt.executeUpdate( "DROP TABLE Metadata" );
			//stmt.executeUpdate( "DROP TABLE Article_Counts" );
			//stmt.executeUpdate( "DROP TABLE Multitoken_Dict" );
			createStopWordsTable();
			System.out.println("StopWords created");
			createArticle_CountsTable();
			System.out.println("Article counts created");
			createMatchTable();
			System.out.println("Match created");
			createMultitoken_DictTable();
			System.out.println("Dict");
			startTime = System.currentTimeMillis();

			ResultSet rs = stmt
					.executeQuery("SELECT * FROM Article_Counts  WHERE Article = 'Urretxu'");
			endTime = System.currentTimeMillis();
			total = (endTime - startTime);
			System.out.println("Query time(ms) " + total);
			while (rs.next()) {
				String name = rs.getString("Article");
				System.out.println(name);
				System.out.println(rs.getInt("Count"));
				String count = rs.getString("Bow");
				System.out.println(count);
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	public static void createMatchTable() throws IOException {

		/**
		 * Class created to take advantage of tree set sorting advantages.
		 */
		class Multitoken implements Comparable<Multitoken> {
			private String multitoken;

			public Multitoken(String s) {
				this.multitoken = s;
			}

			@Override
			public int compareTo(Multitoken arg0) {
				int lengtha = this.multitoken.split(" ").length;
				int lengthb = arg0.getMultitoken().split(" ").length;
				if (lengtha > lengthb)
					return 1;
				else if (lengthb > lengtha)
					return -1;
				return 1;
			}

			public String getMultitoken() {
				return multitoken;
			}

		}
		try {
			stmt.executeUpdate("CREATE TABLE Match ( Token nvarchar, MultiTokens ntext, PRIMARY KEY(Token))");
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(
									new File(
											"/gscratch2/users/abarrena014/sfAndTotalCounts_en")),
							"UTF-8"));
			HashMap<String, Set<Multitoken>> entries = new HashMap<String, Set<Multitoken>>();
			String lerroa = reader.readLine();
			while (lerroa != null) {
				String[] line = lerroa.split("\t");
				// If not a multitoken continue
				if (Integer.parseInt(line[1]) < 1) {
					lerroa = reader.readLine();
					continue;
				}
				// Get the tokens
				String[] sf = line[0].split(" ");
				if (sf.length == 0) {
					System.out.println(lerroa);
					lerroa = reader.readLine();
					continue;
				}
				if (entries.containsKey(sf[0])) {
					Set<Multitoken> multitokenSet = entries.get(sf[0]);
					multitokenSet.add(new Multitoken(line[0]));
				} else {
					Set<Multitoken> multitokenSet = new TreeSet<Multitoken>();
					multitokenSet.add(new Multitoken(line[0]));
					entries.put(sf[0], multitokenSet);
				}
				lerroa = reader.readLine();
			}
			reader.close();

			for (Map.Entry<String, Set<Multitoken>> entry : entries.entrySet()) {
				String mts = new String();
				Set<Multitoken> multitokenSet = entry.getValue();
				Iterator<Multitoken> li = multitokenSet.iterator();
				while (li.hasNext()) {
					mts = li.next().getMultitoken().replace("\'", "\'\'")
							+ "%%%" + mts;
				}
				stmt.executeUpdate("INSERT INTO Match (Token , Multitokens) VALUES ( N'"
						+ entry.getKey().replace("\'", "\'\'")
						+ "',N'"
						+ mts
						+ "')");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void createArticle_CountsTable() throws IOException {
		try {
			stmt.executeUpdate("CREATE TABLE Metadata ( Item varchar, Data text, PRIMARY KEY(Item))");
			stmt.executeUpdate("CREATE TABLE Article_Counts ( Article nvarchar, Count int,Bow ntext , PRIMARY KEY(Article))");

			BufferedReader readCounts = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(
									new File(
											"/gscratch2/users/jcampos004/data/outputErraldoia/uriCounts_en")),
							"UTF-8"));
			int M = 0;
			int N = 0;
			// Get the article counts into the hash
			HashMap<String, Integer> entry = new HashMap<String, Integer>();
			String lerroa = readCounts.readLine();
	
			while (lerroa != null) {
			    if(N%100000==0)
				System.out.println(N + " articles parsed");
				String[] line = lerroa.split("\t");
				String uri;
				
				uri = line[0].replace("quot;" , "\"");
				
				Integer count = entry.get(uri);
				if (count == null)
					entry.put(uri, Integer.parseInt(line[1]));
				else
					entry.put(uri, count + Integer.parseInt(line[1]));
				lerroa = readCounts.readLine();
				M += Integer.parseInt(line[1]);
				N++;
			}
			readCounts.close();

			// Write M and N values to the Metadata table of the database
			try {
				stmt.executeUpdate("INSERT INTO Metadata (Item , Data) VALUES ( 'M','"
						+ M + "')");
				stmt.executeUpdate("INSERT INTO Metadata (Item , Data) VALUES ( 'N','"
						+ N + "')");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			// Get the context of the articles and write them in the
			// article_counts table
			BufferedReader readContext = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(
									new File(
											"/gscratch2/users/jcampos004/data/outputErraldoia/tokenCounts_en")),
							"UTF-8"));
			lerroa = readContext.readLine();
			int lcount = 0;
			while (lerroa != null) {
				String[] line = lerroa.split("\t");
				String uri;
						
				uri = line[0].replace("quot;" , "\"");
				       

				if (entry.containsKey(uri)) {
					int count = entry.get(uri);
					entry.remove(uri);

					// If not empty context
					if (line[1].length() > 2) {
						String[] multitokenContext = line[1].substring(2,
								line[1].length() - 2).split("\\),\\(");
						HashMap<String, Double> context = new HashMap<String, Double>();
						
						// Get only the keywords but add the non keywords
						// probabilities if the same word appears twice.
						String mt = null;
						double prob = 0d;
						for (String multitoken : multitokenContext) {
							for(int i= multitoken.length()-1;i>0;i--){
								if(multitoken.charAt(i)==','){
									mt = multitoken.substring(0,i);
									prob = Double.parseDouble(multitoken.substring(i+1, multitoken.length()));
									break;
								}
							}							
							try {
							//Detect the KEY multitokens and add the probabilities once separated in tokens.
								if (context.containsKey(mt))
									context.put(
											mt,
											context.get(mt)
													+ prob);

								if (mt.contains("KEY:::")
										&& !stopWords
												.containsKey(mt
														.substring(6))) {
									String[] splitMultitoken = mt
											.substring(6).split("_");
									for (String token : splitMultitoken) {
										if (context.containsKey(token))
											context.put(
													token,
													context.get(token)
															+ prob
															/ splitMultitoken.length);
										else
											context.put(
													token,
													prob
															/ splitMultitoken.length);
									}
								}
							} catch (Exception e) {
								System.out.println(multitoken);
							}
						}

						StringBuilder sb = new StringBuilder();

						// Create a String with all the context
						for (String key : context.keySet()) {
							sb.append("(" + key.replace("\'", "\'\'") + " "
									+ context.get(key) + ")");
							sb.append("%%%");
						}

						String contextString = sb.toString();

						// If non empty after removing non key words
						if (contextString.length() > 0) {
							// Insert the value
							try {
								stmt.executeUpdate("INSERT INTO Article_Counts (Article , Count, Bow) VALUES (N'"
										+ uri.replace("\'", "\'\'")
										+ "','"
										+ count
										+ "',N'"
										+ contextString.substring(0,
												contextString.length() - 3)
										+ "')");
							} catch (SQLException e) {
								e.printStackTrace();
								System.out.println("Repeated primary key "
										+ uri);
							} catch (Exception e) {
								System.out.println(lerroa);
							}
						}
						// If empty context
					} else
						try {
							stmt.executeUpdate("INSERT INTO Article_Counts (Article , Count) VALUES (N '"
									+ uri.replace("\'", "\'\'")
									+ "','"
									+ count
									+ "')");
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				} else {
					System.out.println(line[0] + " " + uri
							+ "String not in hash");
				}
				lcount++;
				if(lcount%100000==0){
				    System.out.println(lcount + " contexts processed");
				}
				lerroa = readContext.readLine();
			}
			readContext.close();
			
			lcount = 0;
			// If remain Articles without context add them
			for (String key : entry.keySet()) {
			    lcount++;
			    if(lcount%100000==0)
				System.out.println(lcount + " Articles without context added");
				try {
					stmt.executeUpdate("INSERT INTO Article_Counts (Article , Count) VALUES (N '"
							+ key.replace("\'", "\'\'")
							+ "','"
							+ entry.get(key) + "')");
				} catch (SQLException e) {
					System.out.println(key);
				} catch (Exception e) {
					System.out.println(lerroa);
				}
			}
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

	}

	public static void createMultitoken_DictTable() throws IOException {
		try {
			stmt.executeUpdate("CREATE TABLE Multitoken_Dict (Multitoken nvarchar, Anchor_Count int,  Token_Count int,  Articles ntext ,PRIMARY KEY(Multitoken))");
			BufferedReader readArticles = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(
									new File(
											"/gscratch2/users/jcampos004/data/outputErraldoia/pairCounts_en")),
							"UTF-8"));
			HashMap<String, String> entry = new HashMap<String, String>();
			String lerroa = readArticles.readLine();
			int lcount = 0;
			while (lerroa != null) {
			    lcount++;
			    if(lcount%100000==0)
				System.out.println(lcount + " pairs processed");
				String[] line = lerroa.split("\t");
				if (line.length < 3) {
					lerroa = readArticles.readLine();
					System.out.println(lerroa);
					System.out.println(line.length);
					continue;
				}
				String uri;
			  
				uri = line[1].replace("quot;" , "\"");
				String mention = line[0].replace("quot;" , "\"");
				if (entry.containsKey(mention))
					entry.put(
							mention,
							entry.get(mention) + ",("
									+ uri.replace("\'", "\'\'") + " " + line[2]
									+ ")");
				else
					entry.put(mention, "(" + uri.replace("\'", "\'\'") + " "
							+ line[2] + ")");
				lerroa = readArticles.readLine();
			}
			readArticles.close();
			BufferedReader readCounts = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(
									new File(
											"/gscratch2/users/jcampos004/data/outputErraldoia/sfAndTotalCounts_en")),
							"UTF-8"));
			lerroa = readCounts.readLine();
			lcount = 0;
			while (lerroa != null) {
			    lcount++;
			    if(lcount%100000==0)
				System.out.println(lcount + " multitokens added");
				String[] line = lerroa.split("\t");
				line[0] = line[0].replace("\'", "\'\'").replace("quot;" , "\"");
				if (entry.containsKey(line[0])) {
					String articles = entry.get(line[0]);
					try {
						stmt.executeUpdate("INSERT INTO Multitoken_Dict (Multitoken , Anchor_Count ,  Token_Count ,  Articles ) VALUES (N '"
								+ line[0]
								+ "','"
								+ line[1]
								+ "','"
								+ line[2]
								+ "',N'" + articles + "')");
					} catch (SQLException e) {
						System.out.println(line[0] + " Primary key repeated");
					}
				} else {
					try {
						stmt.executeUpdate("INSERT INTO Multitoken_Dict (Multitoken, Anchor_Count ,  Token_Count  ) VALUES (N '"
								+ line[0]
								+ "','"
								+ line[1]
								+ "','"
								+ line[2]
								+ "')");
					} catch (SQLException e) {
						System.out.println(line[0] + "Primary key repeated");
					}
				}
				lerroa = readCounts.readLine();
			}
			readCounts.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void createStopWordsTable() {
		try {
			stmt.executeUpdate("CREATE TABLE Stopwords ( Language varchar, Stopword_List text ,PRIMARY KEY(Language))");
			try {
				BufferedReader readStopWords = new BufferedReader(
						new FileReader("stopwords.en.list"));
				String lerroa = readStopWords.readLine();
				StringBuilder sb = new StringBuilder();
				while (lerroa != null) {
					stopWords.put(lerroa, 1);
					sb.append(lerroa);
					sb.append(",");
					lerroa = readStopWords.readLine();
				}
				readStopWords.close();
				stmt.executeUpdate("INSERT INTO Stopwords (Language , Stopword_List ) VALUES ( 'en','"
						+ sb.toString().substring(0, sb.length() - 1)
								.replace("\'", "\'\'") + "')");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
